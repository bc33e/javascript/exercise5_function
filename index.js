// BÀI 1
// Khai báo biến dùng chung
const KHU_VUC_A = "kvA";
const KHU_VUC_B = "kvB";
const KHU_VUC_C = "kvC";
const DOI_TUONG_1 = "dt1";
const DOI_TUONG_2 = "dt2";
const DOI_TUONG_3 = "dt3";
// hàm xét ưu tiên khu vực
function diemUuTienKhuVuc(UuTienKhuVuc) {
  if (UuTienKhuVuc == KHU_VUC_A) {
    return 2;
  }
  if (UuTienKhuVuc == KHU_VUC_B) {
    return 1;
  }
  if (UuTienKhuVuc == KHU_VUC_C) {
    return 0.5;
  }
  return 0;
}

// hàm xét ưu tiên đối tượng
function diemUuTienDoiTuong(UuTienDoiTuong) {
  switch (UuTienDoiTuong) {
    case DOI_TUONG_1: {
      return 2.5;
    }
    case DOI_TUONG_2: {
      return 1.5;
    }
    case DOI_TUONG_3: {
      return 1;
    }
    default:
      return 0;
  }
}

document.getElementById("btn-tuyen-sinh").onclick = function () {
  event.preventDefault();
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVuc = document.getElementById("slt-khu-vuc").value;
  var doiTuong = document.getElementById("slt-doi-tuong").value;
  var diemMon1 = document.getElementById("diem-mon-1").value * 1;
  var diemMon2 = document.getElementById("diem-mon-2").value * 1;
  var diemMon3 = document.getElementById("diem-mon-3").value * 1;
  console.log({ diemChuan, khuVuc, doiTuong, diemMon1, diemMon2, diemMon3 });

  var diemCongKhuVuc = diemUuTienKhuVuc(khuVuc);
  console.log("Điểm ưu tiên khu vực: ", diemCongKhuVuc);

  var diemCongDoiTuong = diemUuTienDoiTuong(doiTuong);
  console.log("Điểm ưu tiên đối tượng: ", diemCongDoiTuong);

  var ketQuaTuyenSinh = document.getElementById("kq-tuyen-sinh");
  var tongDiem = 0;
  tongDiem = diemMon1 + diemMon2 + diemMon3 + diemCongKhuVuc + diemCongDoiTuong;
  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    ketQuaTuyenSinh.innerHTML = "Kết quả: Rớt, do có môn bị điểm liệt";
  } else if (tongDiem >= diemChuan && (diemMon1, diemMon2, diemMon3) != 0) {
    ketQuaTuyenSinh.innerHTML = `Kết quả: Đậu - Số điểm là: ${tongDiem}`;
  } else {
    ketQuaTuyenSinh.innerHTML = `Kết quả: Rớt - Số điểm là: ${tongDiem}`;
  }
};

// BÀI 2

const GIA_TIEN_50KW_DAU = 500;
const GIA_TIEN_50KW_TIEP = 650;
const GIA_TIEN_100KW_TIEP = 850;
const GIA_TIEN_150KW_TIEP = 1100;
const GIA_TIEN_CON_LAI = 1300;

document.getElementById("btn-tien-dien").onclick = function () {
  var hoTen = document.getElementById("txt-ho-ten").value;
  var soKwDaDung = document.getElementById("txt-so-kw").value * 1;
  console.log({ hoTen, soKwDaDung });

  var soTienPhaiTra = 0;
  if (soKwDaDung <= 50) {
    soTienPhaiTra = GIA_TIEN_50KW_DAU * soKwDaDung;
  } else if (soKwDaDung <= 100) {
    soTienPhaiTra =
      50 * GIA_TIEN_50KW_DAU + (soKwDaDung - 50) * GIA_TIEN_50KW_TIEP;
  } else if (soKwDaDung <= 200) {
    soTienPhaiTra =
      50 * GIA_TIEN_50KW_DAU +
      50 * GIA_TIEN_50KW_TIEP +
      (soKwDaDung - 100) * GIA_TIEN_100KW_TIEP;
  } else if (soKwDaDung <= 350) {
    soTienPhaiTra =
      50 * GIA_TIEN_50KW_DAU +
      50 * GIA_TIEN_50KW_TIEP +
      100 * GIA_TIEN_100KW_TIEP +
      (soKwDaDung - 200) * GIA_TIEN_150KW_TIEP;
  } else {
    soTienPhaiTra =
      50 * GIA_TIEN_50KW_DAU +
      50 * GIA_TIEN_50KW_TIEP +
      100 * GIA_TIEN_100KW_TIEP +
      150 * GIA_TIEN_150KW_TIEP +
      (soKwDaDung - 350) * GIA_TIEN_CON_LAI;
  }

  console.log("soTienPhaiTra: ", soTienPhaiTra);
  document.getElementById(
    "tien-dien"
  ).innerHTML = `Họ và tên: ${hoTen} - Số tiền phải trả: ${soTienPhaiTra.toLocaleString()} VND`;
};

// BÀI 3
const THUE_DEN_60 = 0.05;
const THUE_60_DEN_120 = 0.1;
const THUE_120_DEN_210 = 0.15;
const THUE_210_DEN_384 = 0.2;
const THUE_384_DEN_624 = 0.25;
const THUE_624_DEN_960 = 0.3;
const THUE_TREN_960 = 0.35;

document.getElementById("btn-tinh-thue").onclick = function () {
  var tenNguoiNop = document.getElementById("txt-ten").value;
  var tongThuNhap = document.getElementById("txt-thu-nhap").value * 1;
  var soNguoiPhuThuoc =
    document.getElementById("txt-nguoi-phu-thuoc").value * 1;
  console.log({ tenNguoiNop, tongThuNhap, soNguoiPhuThuoc });

  var thuNhapChiuThue = tongThuNhap - 4e6 - soNguoiPhuThuoc * 1.6e6;
  var tienThue = 0;
  if (thuNhapChiuThue <= 60e6) {
    tienThue = thuNhapChiuThue * THUE_DEN_60;
  } else if (thuNhapChiuThue <= 120e6) {
    tienThue = 60e6 * THUE_DEN_60 + (thuNhapChiuThue - 60e6) * THUE_60_DEN_120;
  } else if (thuNhapChiuThue <= 210e6) {
    tienThue =
      60e6 * THUE_DEN_60 +
      60e6 * THUE_60_DEN_120 +
      (thuNhapChiuThue - 90e6) * THUE_120_DEN_210;
  } else if (thuNhapChiuThue <= 384e6) {
    tienThue =
      60e6 * THUE_DEN_60 +
      60e6 * THUE_60_DEN_120 +
      90e6 * THUE_120_DEN_210 +
      (thuNhapChiuThue - 174e6) * THUE_210_DEN_384;
  } else if (thuNhapChiuThue <= 624e6) {
    tienThue =
      60e6 * THUE_DEN_60 +
      60e6 * THUE_60_DEN_120 +
      90e6 * THUE_120_DEN_210 +
      174e6 * THUE_384_DEN_624 +
      (thuNhapChiuThue - 240e6) * THUE_384_DEN_624;
  } else if (thuNhapChiuThue <= 960e6) {
    tienThue =
      60e6 * THUE_DEN_60 +
      60e6 * THUE_60_DEN_120 +
      90e6 * THUE_120_DEN_210 +
      174e6 * THUE_384_DEN_624 +
      240e6 * THUE_384_DEN_624 +
      (thuNhapChiuThue - 336e6) * THUE_624_DEN_960;
  } else {
    60e6 * THUE_DEN_60 +
      60e6 * THUE_60_DEN_120 +
      90e6 * THUE_120_DEN_210 +
      174e6 * THUE_384_DEN_624 +
      240e6 * THUE_384_DEN_624 +
      336e6 * THUE_624_DEN_960 +
      (thuNhapChiuThue - 960e6) * THUE_TREN_960;
  }

  document.getElementById(
    "tien-thue"
  ).innerHTML = `Họ và tên: ${tenNguoiNop} - Tiền thuế: ${tienThue} VND`;
};

// BÀI 4
function loaiKhachHang() {
  var chonKhachHang = document.getElementById("slt-khach-hang").value;
  if (chonKhachHang == "doanhNghiep") {
    document.getElementById("txt-ket-noi").style.display = "block";
  } else {
    document.getElementById("txt-ket-noi").style.display = "none";
  }
}

const NHA_DAN = "nhaDan";
const DOANH_NGHIEP = "doanhNghiep";
function phiXuLyHoaDon(khachHang) {
  if (khachHang == NHA_DAN) {
    return 4.5;
  }
  return 15;
}

function phiDichVu(khachHang) {
  if (khachHang == NHA_DAN) {
    return 20.5;
  }
  return 75;
}

function phiThueKenh(khachHang) {
  if (khachHang == NHA_DAN) {
    return 7.5;
  }
  return 50;
}

document.getElementById("btn-tien-cap").onclick = function () {
  var chonKhachHang = document.getElementById("slt-khach-hang").value;
  var maKhachHang = document.getElementById("txt-ma-kh").value;
  var soKenh = document.getElementById("txt-so-kenh").value * 1;
  var soKetNoi = document.getElementById("txt-ket-noi").value * 1;
  console.log({ chonKhachHang, maKhachHang, soKenh, soKetNoi });
  var tienXuLyHoaDon = phiXuLyHoaDon(chonKhachHang);
  var tienDichVu = phiDichVu(chonKhachHang);
  var tienThueKenh = phiThueKenh(chonKhachHang);
  var tienCap = 0;
  if (chonKhachHang == "nhaDan") {
    tienCap = tienXuLyHoaDon + tienDichVu + tienThueKenh * soKenh;
  } else {
    if (soKetNoi <= 10) {
      tienCap = tienXuLyHoaDon + tienDichVu + tienThueKenh * soKenh;
    } else {
      tienCap =
        tienXuLyHoaDon +
        tienDichVu +
        5 * (soKetNoi - 10) +
        tienThueKenh * soKenh;
    }
  }
  console.log("tienCap: ", `${tienCap} $`);
  document.getElementById(
    "tien-cap"
  ).innerHTML = `Mã khách hàng: ${maKhachHang} - Tiền cáp: ${tienCap.toFixed(
    2
  )} $`;
};
